Pod::Spec.new do |s|
  s.name             = 'CustomDropDown'
  s.version          = '0.1.0'
  s.summary          = 'Create CustomDropDown with button and auto layout.'
  s.homepage         = 'https://gitlab.com/mahvish10/customdropdownpod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mahvish10' => 'mahvishsyed15@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/mahvish10/customdropdownpod.git', :branch=> "main" }
  s.ios.deployment_target = '14.0'
  s.swift_version = '4.2'
  s.source_files = 'CustomDropDown/Classes/**/*.{swift,.h,.m}'
  s.resource_bundles = {
            'CustomDropDown' => ['CustomDropDown/Classes/**/*.{xib,storyboard,xcassets}']
  }
  s.dependency 'SwiftyJSON', '~> 4.0'
  s.dependency 'SnapKit'
  s.dependency 'Generics'
end
